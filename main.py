import wsgiref.handlers
from google.appengine.api import memcache
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext.webapp import template
from google.appengine.ext import db
import urllib, urllib2
import string, os, re
from Bio import Entrez
from Bio import Medline
from urlopener import URLOpener


def pubmed_papers_by_author(papers, author):
    author = string.lower(author).split("%2c")
    tmp = []
    for x in author:
        tmp.append("(" + x + "[AUTH])")
    author = " OR ".join(tmp)
    records = memcache.get(author)
    if records is not None:
        return records
    else:
        Entrez.email = "aryee@jhu.edu"     # Always tell NCBI who you are
        handle = Entrez.esearch(db="pubmed", term=author, field="auth", usehistory="y")
        result = Entrez.read(handle)
        webenv = result["WebEnv"]
        query_key = result["QueryKey"]
        handle = Entrez.efetch(db="pubmed", WebEnv=webenv, query_key=query_key, rettype="medline", retmode="text")
        records = list(Medline.parse(handle))
        for rec in records:
            papers.append(rec)
        memcache.add(author, records, 60*60) # Cache results for 60min
        return papers
    

def pubmed_papers_by_collection(papers, collection_id):
    collection_url = "http://www.ncbi.nlm.nih.gov/sites/myncbi/collections/public/" + collection_id
    records = memcache.get(collection_id)
    if records is not None:
        return records
    else:
        Entrez.email = "aryee@jhu.edu"     # Always tell NCBI who you are
        opener = URLOpener()
        resp = opener.open(collection_url)
        url = "http://www.ncbi.nlm.nih.gov/sites/entrez?db=pubmed&cmd=historysearch&querykey=1&report=MEDLINE&format=text&dispmax=1000"
        resp = opener.open(url)
        res = resp.content
        #res = re.sub(re.compile(".*<pre>", re.DOTALL), "",  res)
        #res = re.sub("</pre>.*", "", res)
        res = res.split("\n")
        pmid = []
        for line in res:
            if re.match("PMID", line):
                pmid.append(line.split("-")[1].strip())
        handle = Entrez.efetch(db="pubmed",id=pmid,rettype="medline",retmode="text")
        records = list(Medline.parse(handle))
        for rec in records:
            papers.append(rec)
        memcache.add(collection_id, records, 5) # Cache results for 5 seconds
        return papers
 


def format_authors(author_list, format="pubmed", max_authors=10, highlight=None):
    if isinstance(max_authors, int) != True:
        max_authors = len(author_list)
    if format=="pubmed":
        if highlight==None:
            ret = ", ".join(author_list[0:max_authors])
        else:            
            ret=""
            for author in author_list[0:max_authors]:
                if string.lower(author)==string.lower(highlight):
                    ret = ret + "<au_highlight>" + author + "</au_highlight>, "
                else: 
                    ret = ret + author + ", "
            ret = ret[0:-2]
    if max_authors < len(author_list):
        ret = ret + " and others"        
    return ret


# See http://www.nlm.nih.gov/bsd/mms/medlineelements.html for field descriptions
def format_papers(papers, params):
    for i in range(len(papers)):
        if papers[i].has_key("AU"):
            papers[i]["AU"] = format_authors(papers[i]["AU"], 
                max_authors=params["max_authors"], 
                highlight=params["highlight"])
        else:
            papers[i]["AU"] = ""
    return papers



def parse_params(param_string=None):
    params = dict(author="",
                  max_authors=10,
                  highlight="",
                  collection="",
                  footer="true",
                  rettype="js") # Defaults
    if param_string is not None:
        param_list = param_string.split("%26") #  %26: &
        for param in param_list:
            if re.search("%3D", param):
                key, value = param.split("%3D") #  %3D: =
                params[key] = value
        params["author"] = string.replace(params["author"], "_", " ")
        if params["highlight"]=="":
            params["highlight"] = params["author"]
        else:      
            params["highlight"] = string.replace(params["highlight"], "_", " ")    
        params["max_authors"] = int(params["max_authors"])
    return params

class Search(webapp.RequestHandler):
    def get(self, param_string):
        params = parse_params(param_string)
        papers = []
        if params["author"] is not "":
            papers = pubmed_papers_by_author(papers, params["author"])
        if params["collection"] is not "":
            papers = pubmed_papers_by_collection(papers, params["collection"])
        papers = format_papers(papers, params)
        #self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        if params["rettype"]=="js":
            path = os.path.join(os.path.dirname(__file__), "templates/mypapers.js")
        else:
            path = os.path.join(os.path.dirname(__file__), "templates/search_results.html")
        template_values = dict(papers=papers, params=params)
        self.response.out.write(template.render(path, template_values))

class Home(webapp.RequestHandler):
    def get(self):
        path = os.path.join(os.path.dirname(__file__), "templates/index.html")
        self.response.out.write(template.render(path, {}))        

class About(webapp.RequestHandler):
    def get(self):
        path = os.path.join(os.path.dirname(__file__), "templates/about.html")
        self.response.out.write(template.render(path, {}))        

class Faq(webapp.RequestHandler):
    def get(self):
        path = os.path.join(os.path.dirname(__file__), "templates/faq.html")
        self.response.out.write(template.render(path, {}))        


# Not used. This is now implemented in client-side Javascript instead
class PaperListCode(webapp.RequestHandler):
    def post(self):
        r = self.request
        fields = {}
        fields["author"] = string.lower(re.sub(" ", "_", r.get('author')))
        fields["max_authors"] = r.get('max_authors')
        fields["highlight"] = string.lower(re.sub(" ", "_", r.get('highlight')))
        fields["collection"] = r.get('collection_id')
        fields["footer"] = r.get('footer')
        for key in fields.keys():
            if fields[key] == "":
                #fields[key] = None
                True
        template_values = dict(fields=fields)
        path = os.path.join(os.path.dirname(__file__), "templates/paperlistcode.txt")
        #self.response.headers['Content-Type'] = 'plain/text' 
        self.response.out.write(template.render(path, template_values))        




class Memcache(webapp.RequestHandler):
    def get(self):
        stats = memcache.get_stats()
        template_values = dict(stats=stats)
        path = os.path.join(os.path.dirname(__file__), "templates/memcache_stats.html")
        self.response.out.write(template.render(path, template_values))
            
class HelloWorld(webapp.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('Hello, webapp World!')
    


application = webapp.WSGIApplication(
    [('/hello', HelloWorld), 
     ('/memcache', Memcache),
     ('/search/(.*)', Search), 
     ('/about', About), 
     ('/faq', Faq), 
     ('/', Home)],
    debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()


