function updateUrl() {
    var author = document.getElementById('author').value;
    var collection = document.getElementById('collection').value;
    var max_authors = document.getElementById('max_authors').value;
    var footer = document.getElementById('footer');
    //var url = "http://my-papers.appspot.com/search/";
    var url = "search/";
	if (author != "") {
	    author = author.toLowerCase();
	    author = author.replace(/^ +/g, "");
	    author = author.replace(/ +$/g, "");
	    author = author.replace(/ +/g, " ");
	    author = author.replace(/ *, */g, ",");
	    author = author.replace(/ /g, "_");
	    url = url + "author=" + author + "&";
    }
	if (collection != "") {
	    collection = collection.replace(/.*public/, "");
	    collection = collection.replace(/\//g, "");
	    url = url + "collection=" + collection + "&";
    }

    url = url + "max_authors=" + max_authors + "&";
    if (footer.checked == false) {
        url = url + "footer=false&";
    }
    // Drop the trailing &
    l = url.length;
    if (url.substring(l-1) == "&") {
        url = url.substring(0, l-1);
    }
    code ="<pre><code><small>";
    code = code + "&lt;script type=text/javascript src=\"" + document.location.href + url + "\"></script>\n";
	code = code + "&lt;div id=\"mypapers\"&gt;Loading papers...&lt;/div&gt;";
	code = code + "</small></code></pre>";

    var codeBoxTitle = document.getElementById('search_url_title');
    codeBoxTitle.style.visibility = "visible";
    var previewTitle = document.getElementById('preview_title');
    previewTitle.style.visibility = "visible";
    var codeBox = document.getElementById('search_url');
	codeBox.innerHTML = code;
    preview(url);

}




function preview(url) {
    url = url + "&rettype=web";
		// Get browser-specific AJAX request object
	var xmlhttp;  
		try{
			// Opera 8.0+, Firefox, Safari
			xmlhttp = new XMLHttpRequest();
		} catch (e){
			// Internet Explorer Browsers
			try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					// Something went wrong
					alert("Your browser does not support the Javascript needed to load the publication list!");
					return false;
				}
			}
		}

    var preview = document.getElementById('preview');
	preview.innerHTML = "Loading search results...";	

	xmlhttp.open("GET", url, true);
	xmlhttp.send();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			var preview = document.getElementById('preview');
			preview.innerHTML = xmlhttp.responseText;
		}
	}
	
}

